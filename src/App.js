import logo from './logo.svg';
import './App.css';
import {BrowserRouter, Routes,Route,NavLink} from 'react-router-dom'
import HomePage from './homepage';
import LoginPage from './loginpage';
import ContactPage from './contactpage';
import AboutPage from './aboutpage';

  
function App() {
  return (
    <div className="App">
        <BrowserRouter>
              <ul>
                <li>
                    <NavLink to="/">Home</NavLink>
                </li>
                <li>
                    <NavLink to="/about">about</NavLink>
                </li>
                <li>
                    <NavLink to="/contact">contact</NavLink>
                </li>
                <li>
                    <NavLink to="/login">Login</NavLink>
                </li>
              </ul>
              <Routes>
                   <Route path="/" element={<HomePage/>} />
                   <Route path="/about" element={<AboutPage/>} />
                   <Route path="/contact" element={<ContactPage/>} />
                   <Route path="/login" element={<LoginPage/>} />
              </Routes>
        </BrowserRouter>
    </div>
  );
}

export default App;
